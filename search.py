# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
import searchAgents
class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions) -> object:
        """
        Returns the cost of a particular sequence of actions. If those actions
        include an illegal move, return 999999.
        """

        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]


def no_info_search(problem, fringe):
    fringe.push((problem.getStartState(), []))
    closed = []

    while not fringe.isEmpty():
        node = fringe.pop()
        currentState = node[0]
        currentPath = node[1]

        closed.append(currentState)

        for successor in problem.getSuccessors(currentState):
            state = successor[0]
            direction = successor[1]

            path = list(currentPath)
            path.append(direction)

            if problem.isGoalState(state):
                return path

            if state not in closed:
                fringe.push((state, path))

    return []


def search_with_info(problem, allPaths):
    explored = []
    allPaths.push([(problem.getStartState(),"Stop", 0)])

    while not allPaths.isEmpty():
        path = allPaths.pop()

        last_place = path[-1][0]

        if problem.isGoalState(last_place):
            return [x[1] for x in path][1:]

        if last_place not in explored:
            explored.append(last_place)

            for next_place in problem.getSuccessors(last_place):
                if next_place[0] not in explored:
                    path_till_now = path[:]
                    path_till_now.append(next_place)
                    allPaths.push(path_till_now)

    return []


def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    """
    "*** YOUR CODE HERE ***"
    return no_info_search(problem, util.Stack())


def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    return no_info_search(problem, util.Queue())


def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"

    def cost(a_path):
        costs = []

        for place in a_path:
            costs.append(place[1])

        return problem.getCostOfActions(costs)


    all_paths = util.PriorityQueueWithFunction(cost)
    return search_with_info(problem, all_paths)


def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0


def search_with_priority_queue(problem, heuristic):
    explored = []
    list_actions = []
    begin = problem.getStartState()
    fringe = util.PriorityQueue()
    fringe.push((begin, list_actions), heuristic(begin, problem))
    while not fringe.isEmpty():
        node, actions = fringe.pop()
        if not node in explored: #Verificando se o nó já foi visitado
            explored.append(node)
            if problem.isGoalState(node):
                return actions
            successors = problem.getSuccessors(node)
            for successor in successors:
                position = successor[0] #par ordenado que indica posição
                direction = successor[1] #North, South, East, West
                newActions = actions + [direction]
                newCost = problem.getCostOfActions(newActions) + heuristic(position, problem) #somando os novos custos dos sucessores (cost + heuristic)
                fringe.push((position, newActions), newCost)
    return []


def aStarSearch(problem, heuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    return search_with_priority_queue(problem, heuristic)



    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()







# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
